<?php

global $db;
global $app;

$groups = $db->getGroups();
$roles = $db->getRoles();

?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb primary-color">
        <li class="breadcrumb-item"><a class="white-text" href="/">Главная</a></li>
        <li class="breadcrumb-item active">Создание нового аккаунта</li>
    </ol>
</nav>
<div class="row">
    <div class="col-12">
        <div class="card mt-1 mr-auto mb-3 ml-auto">
            <h5 class="card-header blue white-text text-center py-4">
                <strong>Создание нового аккаунта</strong>
            </h5>
            <div class="card-body px-lg-5 pt-3">
                <form id="FormRegistration" action="/" method="POST">
                    <div class="form-group">
                        <label for="InputRegistrationEmail">Адрес электронной почты</label>
                        <input type="email" name="email" class="form-control" id="InputRegistrationEmail" placeholder="name@mail.ru" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="InputRegistrationPassword">Пароль</label>
                            <input type="password" name="password" class="form-control" id="InputRegistrationPassword" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="InputRegistrationPasswordRepeat">Повторите пароль</label>
                            <input type="password" class="form-control" id="InputRegistrationPasswordRepeat" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName">Полное имя</label>
                        <input type="text" class="form-control" id="inputName" name="name" placeholder="Иванов Иван Иванович" required>
                    </div>
                    <div class="form-row">
                        <div class="col-12 col-md-6">
                            <label for="SelectGroup">Ваша учебная группа</label>
                            <select id="SelectGroup" class="custom-select" name="group" required>
                                <?php foreach ($groups as $key => $group) : ?>
                                    <?php if ($key == 0) : ?>
                                        <option selected value="<?= $group->id ?>"><?= $group->name ?></option>
                                    <?php else : ?>
                                        <option value="<?= $group->id ?>"><?= $group->name ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label>Ваша роль</label>
                            <?php foreach ($roles as $key => $role) : ?>
                                <div class="form-check ml-3">
                                    <?php if ($key == 0) : ?>
                                        <input class="form-check-input" type="radio" name="role" id="RadioRole<?= $role->id ?>" value="<?= $role->id ?>" checked>
                                        <label class="form-check-label" for="RadioRole<?= $role->id ?>">
                                            <?= $role->name ?>
                                        </label>
                                    <?php else : ?>
                                        <input class="form-check-input" type="radio" name="role" id="RadioRole<?= $role->id ?>" value="<?= $role->id ?>">
                                        <label class="form-check-label" for="RadioRole<?= $role->id ?>">
                                            <?= $role->name ?>
                                        </label>
                                    <?php endif ?>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="form-group d-flex justify-content-center align-items-center mt-3">
                        <button type="submit" name="form-registration" form="FormRegistration" class="btn btn-lg btn-primary">Далее</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>