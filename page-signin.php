<nav aria-label="breadcrumb">
    <ol class="breadcrumb primary-color">
        <li class="breadcrumb-item"><a class="white-text" href="/">Главная</a></li>
        <li class="breadcrumb-item active">Авторизация на сайте</li>
    </ol>
</nav>
<div class="row">
    <div class="col-12">
        <div class="card mt-1 mr-auto mb-3 ml-auto" style="max-width: 500px;">
            <h5 class="card-header blue white-text text-center py-4">
                <strong>Авторизация на сайте</strong>
            </h5>
            <div class="card-body px-lg-5 pt-3">
                <form id="FormLogIn" action="/" method="POST">
                    <div class="form-group">
                        <label for="InputLoginEmail">Адрес электронной почты</label>
                        <input type="email" name="email" class="form-control" id="InputLoginEmail" aria-describedby="emailHelp" required>
                        <small id="emailHelp" class="form-text text-muted">Мы никогда никому не передадим вашу электронную почту.</small>
                    </div>
                    <div class="form-group">
                        <label for="InputLoginPassword">Пароль</label>
                        <input type="password" name="password" class="form-control" id="InputLoginPassword" required>
                    </div>
                    <div class="form-group d-flex flex-column justify-content-center align-items-center mt-3">
                        <span>Нет аккаунта?</span>
                        <a class="btn btn-outline-primary" href="/?page=signup">Регистрация</a>

                    </div>
                    <div class="d-flex justify-content-center align-items-center">
                        <button type="submit" form="FormLogIn" name="form-login" class="btn btn-primary m-3">Далее</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>