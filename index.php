<?php
session_start();
?>

<?php include_once 'db.php' ?>
<?php include_once 'functions.php' ?>
<?php include_once 'functions-post.php' ?>


<!DOCTYPE html>
<html lang="en">
<?php include_once 'head.php'; ?>

<body class="heavy-rain-gradient">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-lg-3 navbar-container ">
                <?php
                // Вертикальное меню
                include_once 'header.php';
                ?>
            </div>
            <div class="col-md-8 col-lg-9 content-container">
                <?php
                // Основной контент страницы
                include_once 'page.php';
                include_once 'footer.php';
                ?>
            </div>
        </div>
    </div>
</body>

</html>