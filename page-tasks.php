<?php
global $app;
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb primary-color">
        <li class="breadcrumb-item"><a class="white-text" href="/">Главная</a></li>
        <li class="breadcrumb-item active">Задания</li>
    </ol>
</nav>
<?php
if ($app->user['role'] == '1') {
    include_once 'page-tasks-role-1.php';
} else {
    include_once 'page-tasks-role-2.php';
}
?>