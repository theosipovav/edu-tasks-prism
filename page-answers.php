<nav aria-label="breadcrumb">
    <ol class="breadcrumb primary-color">
        <li class="breadcrumb-item"><a class="white-text" href="/">Главная</a></li>
        <li class="breadcrumb-item active">Ответы </li>
    </ol>
</nav>
<?php
global $app;
if ($app->user['role'] == 1) {
    include_once 'page-answers-role-1.php';
} else {
    include_once 'page-answers-role-2.php';
}
?>