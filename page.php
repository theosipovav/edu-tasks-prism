<?php

global $app;

// Маршрутизация
$fileRoute = 'page-default.php';
if (isset($_GET["page"])) {
    $route = $_GET["page"];
    if ($app->isLogged) {
        $fileRoute = 'page-' . $route . '.php';
    } else {
        $fileRoute = 'page-default.php';
    }
    if ($route == 'signin') {
        $fileRoute = 'page-' . $route . '.php';
    }
    if ($route == 'signup') {
        $fileRoute = 'page-' . $route . '.php';
    }
    if ($route == 'signout') {
        $fileRoute = 'page-' . $route . '.php';
    }
}
if (!file_exists($fileRoute)) {
    $fileRoute = 'page-404.php';
}
include_once $fileRoute;
