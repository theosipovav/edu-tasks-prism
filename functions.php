<?php


class App
{
    public $messageSuccess;
    public $messageWarning;
    public $messageError;
    public $isLogged;

    /**
     * Авторизованный пользователь
     */
    public $user;

    function __construct()
    {
        global $db;
        $this->isLogged = false;
        if (isset($_SESSION['user'])) {

            $user = $db->getUserById($_SESSION['user']['id']);
            if ($user == false) {
                $this->isLogged = false;
                session_unset();
                $this->setMessageWarning("Не удалось выполнить вход. Возможно, был введен неверный логин или пароль.");
            } else {
                $_SESSION['user'] = [];
                $_SESSION['user']['id'] = $user->id;
                $_SESSION['user']['email'] = $user->email;
                $_SESSION['user']['password'] = $user->password;
                $_SESSION['user']['name'] = $user->name;
                $_SESSION['user']['role'] = $user->role;
                $_SESSION['user']['group'] = $user->group;
                $_SESSION['user']['created_dt'] = $user->created_dt;
                $_SESSION['user']['update_dt'] = $user->update_dt;
                $this->user = $_SESSION['user'];
                $this->isLogged = true;
            }

            $this->user = $_SESSION['user'];
            $this->isLogged = true;
        }

        $this->messageSuccess  = "";
        $this->messageWarning  = "";
        $this->messageError  = "";
    }

    /**
     * Задать сообщение о успешном выполнение действия
     */
    public function setMessageSuccess($text)
    {
        $this->messageSuccess = $text;
        $this->messageWarning = "";
        $this->messageError = "";
    }
    /**
     * Задать сообщение о выполнение действия с предупреждением
     */
    public function setMessageWarning($text)
    {
        $this->messageSuccess = "";
        $this->messageWarning = $text;
        $this->messageError = "";
    }
    /**
     * Задать сообщение о неудачном выполнение действия
     */
    public function setMessageError($text)
    {
        $this->messageSuccess = "";
        $this->messageWarning = "";
        $this->messageError = $text;
    }





    /**
     * Регистрация нового пользователя
     */
    function reg($post)
    {
        global $db;
        $user = $db->findUser($post['email']);
        if ($user != false) {
            $this->setMessageWarning("Пользователь с таким адресом электронной почты уже зарегистрировав");
            return false;
        }
        if ($db->createUser($post['email'], $post['password'], $post['name'], $post['role'], $post['group'])) {
            $user = $db->findUser($post['email'], $post['password']);
            $_SESSION['user'] = [];
            $_SESSION['user']['id'] = $user->id;
            $_SESSION['user']['email'] = $user->email;
            $_SESSION['user']['name'] = $user->name;
            $_SESSION['user']['password'] = $user->password;
            $_SESSION['user']['role'] = $user->role;
            $_SESSION['user']['group'] = $user->group;
            $_SESSION['user']['created_dt'] = $user->created_dt;
            $_SESSION['user']['update_dt'] = $user->update_dt;
            $this->user = $_SESSION['user'];
            $this->isLogged = true;
            $this->setMessageSuccess("Учетная запись успешно создана!");
        } else {
            $this->isLogged = false;
            $this->setMessageError($db->message);
        }
    }
    /**
     * Авторизация
     */
    function login($post)
    {
        global $db;
        $email = $post['email'];
        $password = $post['password'];
        $user = $db->findUser($email, $password);
        if ($user == false) {
            $this->isLogged = false;
            session_unset();
            $this->setMessageWarning("Не удалось выполнить вход. Возможно, был введен неверный логин или пароль.");
        } else {
            $_SESSION['user'] = [];
            $_SESSION['user']['id'] = $user->id;
            $_SESSION['user']['email'] = $user->email;
            $_SESSION['user']['password'] = $user->password;
            $_SESSION['user']['name'] = $user->name;
            $_SESSION['user']['role'] = $user->role;
            $_SESSION['user']['group'] = $user->group;
            $_SESSION['user']['created_dt'] = $user->created_dt;
            $_SESSION['user']['update_dt'] = $user->update_dt;
            $this->user = $_SESSION['user'];
            $this->isLogged = true;
        }
    }
    /**
     * Редактирование профиля пользователя
     */
    function updateProfile($post)
    {
        global $db;
        if ($db->updateUser($post['id'], $post['email'], $post['password'], $post['name'], $post['role'], $post['group'])) {
            $user = $db->findUser($post['email'], $post['password']);
            $_SESSION['user'] = [];
            $_SESSION['user']['id'] = $user->id;
            $_SESSION['user']['email'] = $user->email;
            $_SESSION['user']['name'] = $user->name;
            $_SESSION['user']['password'] = $user->password;
            $_SESSION['user']['role'] = $user->role;
            $_SESSION['user']['group'] = $user->group;
            $_SESSION['user']['created_dt'] = $user->created_dt;
            $_SESSION['user']['update_dt'] = $user->update_dt;
            $this->setMessageSuccess("Учетная запись успешно обновлена!");
        } else {
            $this->setMessageError($db->message);
        }
    }
    /**
     * Получить список заданий учебной группы
     */
    function getTaskForGroup($id)
    {
        global $db;
        $tasks = [];
        $users = $db->getUsersByGroup($id, 1);
        if (count($users) == 0) {
            return $tasks;
        }
        $usersIds = [];
        foreach ($users as $key => $user) {
            $usersIds[] = $user->id;
        }
        $tasks = $db->getTasksByAuthors($usersIds);
        return $tasks;
    }
    /**
     * Добавление нового ответа
     */
    function createAnswer($post)
    {
        global $db;
        if ($db->getAnswerByUserAndTask($post['user'], $post['task']) == false) {
            if ($db->createAnswer($post['user'], $post['task'], $post['res'])) {
                $this->setMessageSuccess("Ответ успешно добавлен!");
            } else {
                $this->setMessageError($db->message);
            }
        } else {
            $this->setMessageWarning("Ответ по данному заданию уже был добавлен ранее");
        }
    }
    /**
     * Обновление нового ответа
     */
    function updateAnswer($post)
    {
        global $db;
        if ($db->updateAnswer($post['id'], $post['res'])) {
            $this->setMessageSuccess("Ответ успешно обновлен!");
        } else {
            $this->setMessageError($db->message);
        }
    }
    /**
     * Проверка задания
     */
    function checkAnswer($answerId, $taskId)
    {
        global $db;
        $answer = $db->getAnswer($answerId);
        if ($answer == false) {
            return false;
        }
        $task = $db->getTask($taskId);
        if ($task == false) {
            return false;
        }
        if ($answer->res == $task->answer) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * Добавить нового задание
     */
    function createTask($post)
    {
        global $db;
        if ($db->createTask($post)) {
            $this->setMessageSuccess("Задание успешно обновлено!");
        } else {
            $this->setMessageError($db->message);
        }
    }
    /**
     * Удалить задание
     */
    function removeTask($id)
    {
        global $db;
        if ($db->removeTask($id)) {
            $this->setMessageSuccess("Задание удалено!");
        } else {
            $this->setMessageError($db->message);
        }
    }
    /**
     * Получить всех пользователей, которые выполняли задания 
     */
    function getUsersInAnswers()
    {
        global $db;
        $users = [];
        foreach ($db->getUsersInAnswer() as $key => $id) {
            $users[] = $db->getUserById($id);
        }
        return $users;
    }
}



$app = new App();
