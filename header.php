<?php
global $app;


?>
<!-- Вертикальное меню -->
<nav class="navbar navbar-expand-md navbar-light">
    <?php if ($app->isLogged) : ?>
        <?php if ($app->user['role'] == 1) : ?>
            <div class="view default-color-dark" style="height: 150px;width: 100%;">>
                <div class="mask pattern-8 d-flex flex-column justify-content-center align-items-center">
                    <p class="white-text"><?= $app->user['name'] ?></p>
                    <a href="/?page=profile" class="btn btn-blue-grey">Профиль</a>
                </div>
            </div>
        <?php else : ?>
            <div class="view primary-color-dark" style="height: 150px;width: 100%;">>
                <div class="mask pattern-8 d-flex flex-column justify-content-center align-items-center">
                    <p class="white-text"><?= $app->user['name'] ?></p>
                    <a href="/?page=profile" class="btn btn-blue-grey">Профиль</a>
                </div>
            </div>
        <?php endif ?>
    <?php endif ?>

    <blockquote class="blockquote bq-primary">
        <p class="bq-title">Основное меню</p>
    </blockquote>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbar">
        <!-- Пункты вертикального меню -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="/">Главная</a>
            </li>
            <?php if ($app->isLogged) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="/?page=tasks">Задания</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/?page=answers">Ответы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/?page=help">Помощь</a>
                </li>
                <li class="nav-item">
                    <button type="submit" name="form-logout" form="FormPost" class="nav-item">Выход</button>
                </li>
            <?php else : ?>
                <li class="nav-item">
                    <a class="nav-link" href="/?page=signin">Авторизоваться</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/?page=signup">Зарегистрироваться</a>
                </li>
            <?php endif ?>

        </ul>
    </div>
</nav>






<form id="FormPost" action="/" method="post"></form>