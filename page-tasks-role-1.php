<?php
global $db;
global $app;

$tasks = $db->getTasks();

?>
<div class="row">
    <div class="col-12">
        <h2>Все опубликованные задания</h2>
    </div>
    <div class="col-12">
        <a href="/?page=task-create" class="btn btn-primary"><i class="fas fa-plus-square"></i> Опубликовать новое задание</a>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        <table class="table table-bordered white">
            <thead class="blue white-text">
                <tr>
                    <th></th>
                    <th>Учебная группа</th>
                    <th>Дата публикации</th>
                    <th>Автор</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tasks as $key => $task) : ?>
                    <tr>
                        <th scope="row">Задание №<?= $task->id ?></th>
                        <td><?= $db->getGroupById($db->getUserById($task->author)->group)->name ?></td>
                        <td> <?= $task->created_dt ?></td>
                        <td> <?= $db->getUserById($task->author)->name ?></td>
                        <td>
                            <div class="d-flex justify-content-center align-items-center">
                                <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-primary">Перейти</a>
                                <?php if ($app->user['role'] == 1) : ?>
                                    <form id="FormTaskRemove<?= $task->id ?>" action="/?page=tasks" method="post">
                                        <input type="hidden" name="id" value="<?= $task->id ?>">
                                    </form>
                                    <button type="submit" form="FormTaskRemove<?= $task->id ?>" name="form-task-remove" class="btn btn-danger btn-confirm ml-1">Удалить</button>
                                <?php endif ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>