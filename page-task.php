<?php
global $db;
$get = $_GET;
$task = $db->getTask($get['id']);
$answer = $db->getAnswerByUserAndTask($_SESSION['user']['id'], $task->id);
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb primary-color">
        <li class="breadcrumb-item"><a class="white-text" href="/">Главная</a></li>
        <li class="breadcrumb-item"><a class="white-text" href="/?page=tasks">Задания</a></li>
        <li class="breadcrumb-item active">Задание №<?= $task->id ?></li>
    </ol>
</nav>

<div class="row">
    <div class="col-12">
        <h5>
            Текст задания:
        </h5>
        <p class="lead"><?= $task->text ?></p>
    </div>
    <div class="col-12">
        <div class="card border-primary mt-3 mb-3">
            <div class="card-header">Ответ</div>
            <div class="card-body text-primary">
                <?php if ($answer == false) : ?>
                    <form action="/?page=task&id=<?= $task->id ?>" id="FormAnswerCreate" method="post" class="d-flex justify-content-center align-items-center">
                        <div class="d-flex flex-grow-1">
                            <label for="InputFormAnswerResult" class="mr-3" style="min-width: 100px;">Ваш ответ:</label>
                            <input class="form-control flex-grow-1" type="text" name="res" id="InputFormAnswerResult">
                            <input type="hidden" name="user" value="<?= $_SESSION['user']['id'] ?>">
                            <input type="hidden" name="task" value="<?= $task->id ?>">
                        </div>
                        <div class="d-flex justify-content-center align-items-center">
                            <button type="submit" name="form-answer-create" class="btn btn-primary mt-3">Добавить</button>
                        </div>
                    </form>
                <?php else : ?>
                    <form action="/?page=task&id=<?= $task->id ?>" id="FormAnswerUpdate" method="post" class="d-flex justify-content-center align-items-center">
                        <div class="d-flex flex-grow-1">
                            <label for="InputFormAnswerResult" class="mr-3" style="min-width: 100px;">Ваш ответ:</label>
                            <input class="form-control flex-grow-1" type="text" name="res" id="InputFormAnswerResult" value="<?= $answer->res ?>">
                            <input type="hidden" name="id" value="<?= $answer->id ?>">
                        </div>
                        <div class="d-flex justify-content-center align-items-center">
                            <button type="submit" name="form-answer-update" class="btn btn-primary mt-3">Обновить</button>
                        </div>
                    </form>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12 col-md-6">
        <div class="d-flex justify-content-center align-items-center bg-light">
            <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <form class="container form-task">
            <input type="hidden" id="inputTask" value="<?= $task->id ?>">
            <div class="form-row">
                <div class="col-md-2">
                    <h5>A</h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xa1" value="<?= $task->xa1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="ya1" value="<?= $task->ya1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="za1" value="<?= $task->za1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>D</h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xa2" value="<?= $task->xa2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="ya2" value="<?= $task->ya2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="za2" value="<?= $task->za2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>B</h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xb1" value="<?= $task->xb1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yb1" value="<?= $task->yb1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zb1" value="<?= $task->zb1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>E</h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xb2" value="<?= $task->xb2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yb2" value="<?= $task->yb2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zb2" value="<?= $task->zb2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>C</h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xc1" value="<?= $task->xc1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yc1" value="<?= $task->yc1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zc1" value="<?= $task->zc1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>F</h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xc2" value="<?= $task->xc2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yc2" value="<?= $task->yc2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zc2" value="<?= $task->zc2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <hr>

            <div class="form-group row">
                <div class="col-12 col-md-6 d-flex flex-column">
                    <h5>Прямая 1</h5>
                    <div class="d-flex">
                        <p class="m-1 mr-3"><strong>L1</strong></p>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="xl1" class="form-control" value="<?= $task->xl1 ?>" required>
                            <label for="">x</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="yl1" class="form-control" value="<?= $task->yl1 ?>" required>
                            <label for="">y</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="zl1" class="form-control" value="<?= $task->zl1 ?>" required>
                            <label for="">z</label>
                        </div>
                    </div>
                    <div class="d-flex">
                        <p class="m-1 mr-3"><strong>L2</strong></p>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="xl2" class="form-control" value="<?= $task->xl2 ?>" required>
                            <label for="">x</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="yl2" class="form-control" value="<?= $task->yl2 ?>" required>
                            <label for="">y</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="zl2" class="form-control" value="<?= $task->zl2 ?>" required>
                            <label for="">z</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 d-flex flex-column">
                    <h5>Прямая 2</h5>
                    <div class="d-flex">
                        <p class="m-1 mr-3"><strong>L3</strong></p>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="xl3" class="form-control" value="<?= $task->xl3 ?>" required>
                            <label for="">x</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="yl3" class="form-control" value="<?= $task->yl3 ?>" required>
                            <label for="">y</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="zl3" class="form-control" value="<?= $task->zl3 ?>" required>
                            <label for="">z</label>
                        </div>
                    </div>
                    <div class="d-flex">
                        <p class="m-1 mr-3"><strong>L4</strong></p>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="xl4" class="form-control" value="<?= $task->xl4 ?>" required>
                            <label for="">x</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="yl4" class="form-control" value="<?= $task->yl4 ?>" required>
                            <label for="">y</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="zl4" class="form-control" value="<?= $task->zl4 ?>" required>
                            <label for="">z</label>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>