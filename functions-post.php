<?php
global $db;
global $app;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    /**
     * Регистрация нового пользователя
     */
    if (isset($_POST['form-registration'])) {
        $app->reg($_POST);
    }
    /**
     * Авторизация
     */
    if (isset($_POST['form-login'])) {
        $app->login($_POST);
    }
    /**
     * Выход
     */
    if (isset($_POST['form-logout'])) {
        session_unset();
        $app->isLogged = false;
    }
    /**
     * Обновление профиля
     */
    if (isset($_POST['form-profile-update'])) {
        $app->updateProfile($_POST);
    }
    /**
     * Добавить ответ
     */
    if (isset($_POST['form-answer-create'])) {
        $app->createAnswer($_POST);
    }
    /**
     * Добавить ответ
     */
    if (isset($_POST['form-answer-update'])) {
        $app->updateAnswer($_POST);
    }
    /**
     * Добавить задание
     */
    if (isset($_POST['form-task-create'])) {
        $app->createTask($_POST);
    }
    /**
     * Удалить задание
     */
    if (isset($_POST['form-task-remove'])) {
        $app->removeTask($_POST['id']);
    }
}
