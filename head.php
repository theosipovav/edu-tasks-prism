<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Учебные задания по теме "Многогранник"</title>
    <link rel="canonical" href="." />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/mdb.min.css" />
    <link rel="stylesheet" href="assets/css/style.min.css" />

    <!-- favicon for desktop -->
    <link type="image/x-icon" rel="shortcut icon" href="assets/img/favicon.ico">
    <link type="image/png" sizes="16x16" rel="icon" href="assets/img/favicon-16x16.png">
    <link type="image/png" sizes="32x32" rel="icon" href="assets/img/favicon-32x32.png">
    <link type="image/png" sizes="96x96" rel="icon" href="assets/img/favicon-96x96.png">
    <link type="image/png" sizes="120x120" rel="icon" href="assets/img/favicon-120x120.png">

    <!-- favicon for android -->
    <link type="image/png" sizes="192x192" rel="icon" href="assets/img/android-icon-192x192.png">

    <!-- favicon for iso -->
    <link sizes="57x57" rel="apple-touch-icon" href="assets/img/apple-touch-icon-57x57.png">
    <link sizes="60x60" rel="apple-touch-icon" href="assets/img/apple-touch-icon-60x60.png">
    <link sizes="72x72" rel="apple-touch-icon" href="assets/img/apple-touch-icon-72x72.png">
    <link sizes="76x76" rel="apple-touch-icon" href="assets/img/apple-touch-icon-76x76.png">
    <link sizes="114x114" rel="apple-touch-icon" href="assets/img/apple-touch-icon-114x114.png">
    <link sizes="120x120" rel="apple-touch-icon" href="assets/img/apple-touch-icon-120x120.png">
    <link sizes="144x144" rel="apple-touch-icon" href="assets/img/apple-touch-icon-144x144.png">
    <link sizes="152x152" rel="apple-touch-icon" href="assets/img/apple-touch-icon-152x152.png">
    <link sizes="180x180" rel="apple-touch-icon" href="assets/img/apple-touch-icon-180x180.png">

    <!-- favicon for mac -->
    <link color="#e52037" rel="mask-icon" href="assets/img/safari-pinned-tab.svg">


</head>