<?php
global $db;
global $app;
$answers = $db->getAnswers();
$filterUsers = $app->getUsersInAnswers();


if (isset($_POST['form-filter'])) {
    /**
     * Фильтр по пользователям
     */
    if (isset($_POST['user']) && $_POST['user'] != "") {
        $answersFilter = [];
        foreach ($answers as $key => $answer) {
            if ($_POST['user'] == $answer->user) {
                $answersFilter[] = $answer;
            }
        }
        $answers = $answersFilter;
    }
    /**
     * Фильтр по статусу
     */
    if (isset($_POST['status']) && $_POST['status'] != "") {
        $answersFilter = [];
        foreach ($answers as $key => $answer) {
            if ($app->checkAnswer($answer->id, $answer->task)) {
                if ($_POST['status'] == 1) {
                    $answersFilter[] = $answer;
                }
            } else {
                if ($_POST['status'] == 2) {
                    $answersFilter[] = $answer;
                }
            }
        }
        $answers = $answersFilter;
    }
}

?>

<div class="row">
    <div class="col-12">
        <h2 class="h2">Результаты выполнения заданий</h2>
    </div>
</div>
<div class="row mb-1">
    <div class="col-12">
        <form action="/?page=answers" id="FormTaskFilter" method="post">
            <div class="d-flex flex-column">
                <div class="d-flex align-items-center">
                    <h4 class="h3 m-1">Фильтр</h4>
                    <div class="d-flex flex-column flex-grow-1 pr-1 pl-1 mr-1 ml-1">
                        <label for="SelectFormTaskFilterUser">Студент</label>
                        <select name="user" id="SelectFormTaskFilterUser" class="form-control">
                            <option disabled selected>Студент</option>
                            <?php foreach ($filterUsers as $key => $user) : ?>
                                <option value="<?= $user->id ?>"><?= $user->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="d-flex flex-column flex-grow-1 pr-1 pl-1 mr-1 ml-1">
                        <label for="SelectFormTaskFilterUser">Статус</label>
                        <select name="status" id="SelectFormTaskFilterUser" class="form-control">
                            <option disabled selected>Статус</option>
                            <option value="1">Решено верно</option>
                            <option value="2">Решено с ошибкой</option>
                        </select>
                    </div>
                </div>

                <div class="d-flex justify-content-center align-items-center">
                    <button type="submit" class="btn btn-sm peach-gradient" form="FormTaskFilter" name="form-filter">Применить</button>
                    <button type="submit" class="btn btn-sm btn-primary" form="FormTaskFilter" name="form-filter">Сбросить</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <table class="table table-sm table-striped table-bordered">
            <thead>
                <tr class="peach-gradient text-white">
                    <th scope="col"><strong>Номер задачи</strong></th>
                    <th scope="col"><strong>Студент</strong></th>
                    <th scope="col"><strong>Дата ответа</strong></th>
                    <th scope="col"><strong>Статус</strong></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($answers as $key => $answer) : ?>
                    <?php if ($app->checkAnswer($answer->id, $answer->task)) : ?>
                        <tr>
                            <td><strong><?= $answer->id ?></strong></td>
                            <td><?= $db->getUserById($answer->user)->name ?></td>
                            <td><?= $answer->update_dt ?></td>
                            <td>
                                <div class="d-flex justify-content-center align-items-center text-success font-weight-bold">
                                    <i class="fas fa-check-circle mr-1"></i>
                                    <span>Решено верно</span>
                                </div>
                            </td>
                            <td class="d-flex justify-content-center align-items-center"><a href="/?page=task&id=<?= $answer->task ?>" class="btn btn-sm btn-info">Задание</a></td>
                        </tr>

                    <?php else : ?>
                        <tr>
                            <td><strong><?= $answer->id ?></strong></td>
                            <td><?= $db->getUserById($answer->user)->name ?></td>
                            <td><?= $answer->update_dt ?></td>
                            <td>
                                <div class="d-flex justify-content-center align-items-center text-danger font-weight-bold">
                                    <i class="fas fa-exclamation-triangle mr-1"></i>
                                    <span>Решено с ошибкой</span>
                                </div>
                            </td>
                            <td class="d-flex justify-content-center align-items-center"><a href="/?page=task&id=<?= $answer->task ?>" class="btn btn-sm btn-info">Задание</a></td>
                        </tr>

                    <?php endif ?>
                <?php endforeach ?>

            </tbody>
        </table>
    </div>
</div>