<?php
global $db;
global $app;
$tasks = $db->getTasks(4);
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb primary-color">
        <li class="breadcrumb-item"><a class="white-text" href="/">Главная</a></li>
    </ol>
</nav>
<div class="row">
    <div class="col-12 text-center">
        <h1 class="display-4">Призма на основе 3D-технологий в Web</h1>
    </div>
</div>
<hr>
<?php if ($app->isLogged) : ?>
    <div class="row">
        <div class="col-12">
            <h3 class="h3">Призма</h3>
            <p>
                При́зма (лат. prisma от др.-греч. πρίσμα «нечто отпиленное») — многогранник, две грани которого являются конгруэнтными (равными) многоугольниками, лежащими в параллельных плоскостях, а остальные грани — параллелограммами, имеющими общие стороны с этими многоугольниками. Эти параллелограммы называются боковыми гранями призмы, а оставшиеся два многоугольника называются её основаниями.
            </p>
            <p>
                Многоугольник, лежащий в основании, определяет название призмы: треугольник — треугольная призма, четырёхугольник — четырёхугольная; пятиугольник — пятиугольная (пентапризма) и т. д.
            </p>
            <p>
                Призма является частным случаем цилиндра в общем смысле (некругового).
            </p>
            <h3 class="h3">
                Треугольная призма
            </h3>
            <p>
                Этот многогранник имеет в качестве граней треугольное основание, его копию, полученную в результате параллельного переноса и 3 грани, соединяющие соответствующие стороны[en]. Прямая треугольная призма имеет прямоугольные боковые стороны, в противном случае призма называется косой.
            </p>
        </div>

    </div>
    <div class="row">
        <div class="col-12">
            <h3 class="h3">Последние задания</h3>
        </div>
    </div>
    <div class="row">
        <?php foreach ($tasks as $key => $task) : ?>

            <div class="col-6 col-md-6 col-lg-3">
                <div class="card d-flex flex-column mt-2 mb-2">
                    <div class="card-title">
                    </div>
                    <div class="card-body">
                        <h4 class="card-title"><a>Задание №<?= $task->id ?></a></h4>
                        <p class="card-text">Группа: <span><?= $db->getGroupById($_SESSION['user']['group'])->name ?></span></p>
                        <p class="card-text">Дата размещения: <span><?= $task->created_dt ?></span></p>
                        <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-primary">Перейти</a>
                    </div>


                </div>
            </div>
        <?php endforeach ?>
    </div>
    <hr>
<?php else : ?>
    <div class="row">
        <div class="col-12">
            <div class="jumbotron text-center">
                <h2 class="card-title h2">Добро пожаловать!</h2>
                <p class="blue-text my-4 font-weight-bold">Уважаемый пользователь, для неавторизованных пользователь функционал сайта недоступен</p>
                <div class="row d-flex justify-content-center">
                </div>
                <hr class="my-4">
                <div class="pt-2">
                    <a type="button" href="/?page=signin" class="btn btn-blue waves-effect">Войти</a>
                    <a type="button" href="/?page=signup" class="btn btn-outline-primary waves-effect">Создать аккаунт</a>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>