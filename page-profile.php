<?php

global $db;
global $app;

$groups = $db->getGroups();
$roles = $db->getRoles();


$groupCurrent = $db->getGroupById($_SESSION['user']['group']);
$roleCurrent = $db->getRole($_SESSION['user']['role']);


?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb primary-color">
        <li class="breadcrumb-item"><a class="white-text" href="/">Главная</a></li>
        <li class="breadcrumb-item active">Профиль пользователя <?= $app->user['name'] ?></li>
    </ol>
</nav>
<div class="row">
    <div class="col-12">
        <div class="card mt-1 mr-auto mb-3 ml-auto" style="max-width: 750px;">

            <?php if ($app->user['role'] == 1) : ?>
                <div class="view default-color-dark" style="height: 150px;width: 100%;">
                    <div class="mask pattern-8 d-flex flex-column justify-content-center align-items-center">
                        <p class="white-text h3"><?= $app->user['name'] ?></p>
                    </div>
                </div>
            <?php else : ?>
                <div class="view primary-color-dark" style="height: 150px;width: 100%;">>
                    <div class="mask pattern-8 d-flex flex-column justify-content-center align-items-center">
                        <p class="white-text h3"><?= $app->user['name'] ?></p>
                    </div>
                </div>
            <?php endif ?>
            <div class="card-body d-flex flex-column">
                <table class="table table-sm table-light">
                    <tbody>
                        <tr>
                            <td>Идентификатор пользователя</td>
                            <td><?= $_SESSION['user']['id'] ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Учебная группа:</th>
                            <td><?= $groupCurrent->name ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Электронная почты:</th>
                            <td><?= $_SESSION['user']['email'] ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Роль:</th>
                            <td><?= $roleCurrent->name ?></td>
                        </tr>
                    </tbody>
                </table>

                <hr>
                <p class="card-text text-right">
                    <span><small class="text-muted">Дата регистрации: </small></span>
                    <span><small class="text-muted"><?= $_SESSION['user']['created_dt'] ?></small></span>
                </p>
                <div class="d-flex justify-content-center align-items-center">
                    <button class="btn btn-outline-primary" style="width: 200px;" data-toggle="modal" data-target="#ModalEditProfile">
                        <i class="fas fa-user-edit"></i>
                        <span>Редактировать</span>
                    </button>
                    <button type="submit" form="FormPost" name="form-logout" class="btn btn-dark mt-1" style="width: 200px;" title="Выход">
                        <i class="fas fa-sign-out-alt"></i>
                        <span>Выход</span>
                    </button>
                </div>
            </div>

        </div>
        <!-- Card -->
    </div>
</div>


<div class="modal fade" id="ModalEditProfile" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="ModalEditProfileLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalEditProfileLabel">Редактирование профиля</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="FormProfileUpdate" action="/?page=profile" method="POST">
                    <div class="form-group">
                        <label for="InputEditProfileEmail">Адрес электронной почты</label>
                        <input type="email" name="email" class="form-control" id="InputEditProfileEmail" placeholder="name@mail.ru" value="<?= $_SESSION['user']['email'] ?>" required>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="InputEditProfilePassword">Пароль</label>
                            <input type="password" name="password" class="form-control" id="InputEditProfilePassword" value="<?= $_SESSION['user']['password'] ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="InputEditProfilePasswordRepeat">Повторите пароль</label>
                            <input type="password" class="form-control" id="InputEditProfilePasswordRepeat" value="<?= $_SESSION['user']['password'] ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InputEditProfileName">Полное имя</label>
                        <input type="text" class="form-control" id="InputEditProfileName" name="name" placeholder="Иванов Иван Иванович" value="<?= $_SESSION['user']['name'] ?>" required>
                    </div>
                    <div class="form-group">
                        <div class="d-flex">
                            <label for="SelectEditProfileGroup" style="min-width: 150px;">Ваша учебная группа</label>
                            <select id="SelectEditProfileGroup" class="custom-select flex-grow-1" name="group" required>
                                <?php foreach ($groups as $key => $group) : ?>
                                    <?php if ($group->id == $groupCurrent->id) : ?>
                                        <option selected value="<?= $group->id ?>"><?= $group->name ?></option>
                                    <?php else : ?>
                                        <option value="<?= $group->id ?>"><?= $group->name ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="d-flex">
                            <label style="min-width: 150px;">Ваша роль</label>
                            <div class="d-flex flex-column flex-grow-1">
                                <?php foreach ($roles as $key => $role) : ?>

                                    <?php if ($app->user['role'] == 1) : ?>
                                        <div class="form-check ml-3">
                                            <?php if ($role->id == $roleCurrent->id) : ?>
                                                <input class="form-check-input" type="radio" name="role" id="RadioEditProfileRole<?= $role->id ?>" value="<?= $role->id ?>" checked>
                                                <label class="form-check-label" for="RadioEditProfileRole<?= $role->id ?>">
                                                    <?= $role->name ?>
                                                </label>
                                            <?php else : ?>
                                                <input class="form-check-input" type="radio" name="role" id="RadioEditProfileRole<?= $role->id ?>" value="<?= $role->id ?>">
                                                <label class="form-check-label" for="RadioEditProfileRole<?= $role->id ?>">
                                                    <?= $role->name ?>
                                                </label>
                                            <?php endif ?>
                                        </div>
                                    <?php else : ?>
                                        <div class="form-check ml-3">
                                            <?php if ($role->id == $roleCurrent->id) : ?>
                                                <input class="form-check-input" type="radio" name="role" id="RadioEditProfileRole<?= $role->id ?>" value="<?= $role->id ?>" disabled checked>
                                                <label class="form-check-label" for="RadioEditProfileRole<?= $role->id ?>">
                                                    <?= $role->name ?>
                                                </label>
                                            <?php else : ?>
                                                <input class="form-check-input" type="radio" name="role" id="RadioEditProfileRole<?= $role->id ?>" value="<?= $role->id ?>" disabled>
                                                <label class="form-check-label" for="RadioEditProfileRole<?= $role->id ?>">
                                                    <?= $role->name ?>
                                                </label>
                                            <?php endif ?>
                                        </div>
                                    <?php endif ?>






                                <?php endforeach ?>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?= $_SESSION['user']['id'] ?>">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Отмена</button>
                <button type="submit" name="form-profile-update" form="FormProfileUpdate" class="btn btn-primary">Сохранить</button>
            </div>
        </div>
    </div>
</div>