<?php
global $db;
global $app;
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb primary-color">
        <li class="breadcrumb-item"><a class="white-text" href="/">Главная</a></li>
        <li class="breadcrumb-item active">Помощь</li>
    </ol>
</nav>
<div class="row">
    <div class="col-12">
        <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
            <div class="card">
                <div class="card-header" role="tab" id="headingOne1">
                    <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                        <h5 class="mb-0">
                            Пример решение №1 <i class="fas fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>
                <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
                    <div class="card-body">
                        <img src="assets/img/example-1.jpg" class="rounded mx-auto d-block" style="height: 500px;">
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" role="tab" id="headingTwo2">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                        <h5 class="mb-0">
                            Пример решение №2 <i class="fas fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>
                <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
                    <div class="card-body">
                        <img src="assets/img/example-2.jpg" class="rounded mx-auto d-block" style="height: 500px;">
                    </div>
                </div>

            </div>
            <div class="card">
                <div class="card-header" role="tab" id="headingThree3">
                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                        <h5 class="mb-0">
                            Пример решение №3 <i class="fas fa-angle-down rotate-icon"></i>
                        </h5>
                    </a>
                </div>
                <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
                    <div class="card-body">
                        <img src="assets/img/example-3.jpg" class="rounded mx-auto d-block" style="height: 500px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>