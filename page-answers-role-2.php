<?php
global $db;
global $app;
$answers = $db->getAnswersByUser($_SESSION['user']['id']);

?>

<div class="row">
    <div class="col-12">
        <h2 class="display-3">Результаты выполнения заданий</h2>
    </div>
    <div class="col-12">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Номер задачи</th>
                    <th scope="col">Дата ответа</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($answers as $key => $answer) : ?>
                    <?php if ($app->checkAnswer($answer->id, $answer->task)) : ?>
                        <tr class="bg-success text-light">
                            <th><?= $key ?></th>
                            <td><strong><?= $answer->id ?></strong></td>
                            <td><?= $answer->update_dt ?></td>
                            <td class="d-flex justify-content-center align-items-center"><a href="/?page=task&id=<?= $answer->task ?>" class="btn btn-sm btn-light">Задание</a></td>
                        </tr>

                    <?php else : ?>
                        <tr class="bg-danger text-light">
                            <th><?= $key ?></th>
                            <td><strong><?= $answer->id ?></strong></td>
                            <td><?= $answer->update_dt ?></td>
                            <td class="d-flex justify-content-center align-items-center"><a href="/?page=task&id=<?= $answer->task ?>" class="btn btn-sm btn-light">Задание</a></td>
                        </tr>

                    <?php endif ?>
                <?php endforeach ?>

            </tbody>
        </table>
    </div>
</div>